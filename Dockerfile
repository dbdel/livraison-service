# Use the official OpenJDK image for Java 17 as the base image
FROM openjdk:17

# Optionally set a maintainer or label for the image
LABEL maintainer="your_email@example.com"

# Set the working directory inside the container
WORKDIR /app

# Copy the JAR file from your host into the container
COPY target/app.jar /app/app.jar

# Expose the port your app runs on
EXPOSE 8080

# Command to run the application
CMD ["java", "-jar", "app.jar"]