package com.example.livraisonservice.repository;

import com.example.livraisonservice.domain.DeliveryMode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryModeRepository extends JpaRepository<DeliveryMode,Long> {
}
