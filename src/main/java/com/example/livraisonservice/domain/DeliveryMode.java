package com.example.livraisonservice.domain;

import com.example.livraisonservice.enums.DeliveryModeEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "DELIVERY_MODE")
public class DeliveryMode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private DeliveryModeEnum mode;

    @OneToMany(mappedBy = "deliveryMode", cascade = CascadeType.ALL)
    private List<TimeSlot> timeSlots;

}