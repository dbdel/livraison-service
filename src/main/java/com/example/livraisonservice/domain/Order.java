package com.example.livraisonservice.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
@Entity
@Getter
@Setter
@Table(name = "\"Order\"")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "DELIVERY_MODE_ID")
    private DeliveryMode deliveryMode;
    @ManyToOne
    @JoinColumn(name = "CLIENT_ID")
    private Client client;

    @ManyToOne
    @JoinColumn(name = "TIME_SLOT_ID")
    private TimeSlot timeSlot;
    @Column(name = "DELIVERY_DATE")
    private LocalDateTime deliveryDateTime;

}
