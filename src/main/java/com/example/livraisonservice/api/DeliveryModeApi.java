package com.example.livraisonservice.api;

import com.example.livraisonservice.domain.DeliveryMode;
import com.example.livraisonservice.dto.DeliveryModeDto;
import com.example.livraisonservice.services.DeliveryModeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/delivery_mode")
@CrossOrigin("*")
@Api(tags = "Delivery Modes", description = "Endpoints for managing delivery modes")
@EnableCaching
public class DeliveryModeApi {

    @Autowired
    private DeliveryModeService deliveryModeService;

    @GetMapping
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @Cacheable(value = "deliveryMode")
    public Mono<ResponseEntity<CollectionModel<EntityModel<DeliveryModeDto>>>> getAllDeliveryModes() {
        Flux<DeliveryModeDto> deliveryModeDTOs = deliveryModeService.getAllDeliveryModes();

        return deliveryModeDTOs
                .map(this::addLinks)
                .collectList()
                .map(deliveryModeModels -> ResponseEntity.ok(CollectionModel.of(deliveryModeModels)));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @Cacheable(value = "deliveryMode", key = "#id")
    public Mono<ResponseEntity<EntityModel<DeliveryModeDto>>> getDeliveryModeById(@PathVariable Long id) {
        Mono<DeliveryModeDto> deliveryModeDTO = deliveryModeService.getDeliveryModeById(id);

        return deliveryModeDTO
                .map(this::addLinks)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    private EntityModel<DeliveryModeDto> addLinks(DeliveryModeDto deliveryModeDTO) {
        EntityModel<DeliveryModeDto> deliveryModeModel = EntityModel.of(deliveryModeDTO);
        deliveryModeModel.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DeliveryModeApi.class).getDeliveryModeById(deliveryModeDTO.getId())).withSelfRel());
        return deliveryModeModel;
    }
}
