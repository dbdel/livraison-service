package com.example.livraisonservice.api;

import com.example.livraisonservice.domain.TimeSlot;
import com.example.livraisonservice.dto.DeliveryModeDto;
import com.example.livraisonservice.dto.TimeSlotDto;
import com.example.livraisonservice.services.TimeSlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/time-slots")
@CrossOrigin("*")
@EnableCaching
public class TimeSlotApi {
    @Autowired
    private TimeSlotService timeSlotService;

    @GetMapping
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @Cacheable(value = "timeSlot")
    public Mono<ResponseEntity<CollectionModel<EntityModel<TimeSlotDto>>>> getAllTimeSlots() {
        Flux<TimeSlotDto> timeSlotDtoFlux = timeSlotService.getAllTimeSlots();
        return timeSlotDtoFlux
                .map(this::addLinks)
                .collectList()
                .map(deliveryModeModels -> ResponseEntity.ok(CollectionModel.of(deliveryModeModels)));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @Cacheable(value = "timeSlot", key = "#id")
    public Mono<ResponseEntity<EntityModel<TimeSlotDto>>> getTimeSlotById(@PathVariable Long id) {
        Mono<TimeSlotDto> timeSlotDtoMono = timeSlotService.getTimeSlotById(id);
        return timeSlotDtoMono
                .map(this::addLinks)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }




    private EntityModel<TimeSlotDto> addLinks(TimeSlotDto timeSlotDto) {
        EntityModel<TimeSlotDto> timeSlotModel = EntityModel.of(timeSlotDto);
        timeSlotModel.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(TimeSlotApi.class).getTimeSlotById(timeSlotDto.getId())).withSelfRel());
        return timeSlotModel;
    }


}
