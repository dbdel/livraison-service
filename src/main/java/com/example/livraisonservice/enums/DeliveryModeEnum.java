package com.example.livraisonservice.enums;

public enum DeliveryModeEnum {
        DRIVE("Drive"),
        DELIVERY("Delivery"),
        DELIVERY_TODAY("Delivery Today"),
        DELIVERY_ASAP("Delivery Asap");

    DeliveryModeEnum(String libelle) {
        this.libelle = libelle;
    }

    public String getLibelle() {
        return libelle;
    }

    private String	libelle;

}
