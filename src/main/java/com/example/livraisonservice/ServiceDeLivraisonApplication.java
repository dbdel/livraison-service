package com.example.livraisonservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceDeLivraisonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceDeLivraisonApplication.class, args);
	}

}
