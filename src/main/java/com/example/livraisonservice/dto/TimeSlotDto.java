package com.example.livraisonservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TimeSlotDto {
    private Long id;
    private LocalDate day;
    private LocalTime startTime;
    private LocalTime endTime;
    private Long deliveryModeId;
}
