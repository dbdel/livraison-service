package com.example.livraisonservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    private Long id;
    private Long deliveryModeId;
    private Long clientId;
    private Long timeSlotId;
    private LocalDateTime deliveryDateTime;
}
