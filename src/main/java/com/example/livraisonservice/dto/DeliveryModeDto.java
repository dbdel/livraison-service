package com.example.livraisonservice.dto;

import com.example.livraisonservice.enums.DeliveryModeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryModeDto {

    private Long id;
    private DeliveryModeEnum mode;
    private List<Long> timeSlotIds;
}
