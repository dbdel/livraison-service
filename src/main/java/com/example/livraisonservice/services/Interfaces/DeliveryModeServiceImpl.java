package com.example.livraisonservice.services.Interfaces;

import com.example.livraisonservice.domain.DeliveryMode;
import com.example.livraisonservice.dto.DeliveryModeDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface DeliveryModeServiceImpl {

    Flux<DeliveryModeDto> getAllDeliveryModes();

    Mono<DeliveryModeDto> getDeliveryModeById(Long id);
}
