package com.example.livraisonservice.services.Interfaces;

import com.example.livraisonservice.domain.TimeSlot;
import com.example.livraisonservice.dto.TimeSlotDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface TimeSlotServiceImpl {
    public Flux<TimeSlotDto>getAllTimeSlots();

    public Mono<TimeSlotDto> getTimeSlotById(Long id);
    public Mono<TimeSlotDto> createTimeSlot(TimeSlotDto timeSlot);

}
