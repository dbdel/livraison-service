package com.example.livraisonservice.services;

import com.example.livraisonservice.domain.TimeSlot;
import com.example.livraisonservice.dto.TimeSlotDto;
import com.example.livraisonservice.mapper.TimeSlotMapper;
import com.example.livraisonservice.repository.TimeSlotRepository;
import com.example.livraisonservice.services.Interfaces.TimeSlotServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TimeSlotService implements TimeSlotServiceImpl {
    @Autowired
    private TimeSlotRepository timeSlotRepository;

    @Autowired
    private TimeSlotMapper timeSlotMapper;
    @Override
    public Flux<TimeSlotDto> getAllTimeSlots() {
        return timeSlotMapper.toDto(Flux.fromIterable(timeSlotRepository.findAll()));
    }

    @Override
    public Mono<TimeSlotDto> getTimeSlotById(Long id) {
        return Mono.fromCallable(() -> timeSlotMapper.timeslotToDTO(timeSlotRepository.findById(id)));
    }

    @Override
    public Mono<TimeSlotDto> createTimeSlot(TimeSlotDto timeSlot) {
            TimeSlot timeS=timeSlotMapper.toEntity(timeSlot);
        return Mono.fromCallable(() -> timeSlotMapper.timeslotToDTO(timeSlotRepository.save(timeS)));
    }


}
