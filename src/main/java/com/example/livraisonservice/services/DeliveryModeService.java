package com.example.livraisonservice.services;

import com.example.livraisonservice.domain.DeliveryMode;
import com.example.livraisonservice.dto.DeliveryModeDto;
import com.example.livraisonservice.mapper.DeliveryModeMapper;
import com.example.livraisonservice.repository.DeliveryModeRepository;
import com.example.livraisonservice.services.Interfaces.DeliveryModeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class DeliveryModeService implements DeliveryModeServiceImpl {
    @Autowired
    private DeliveryModeRepository deliveryModeRepository;
    @Autowired
    private DeliveryModeMapper deliveryModeMapper;


    @Override
    public Flux<DeliveryModeDto> getAllDeliveryModes() {
        return deliveryModeMapper.toDto(Flux.fromIterable(deliveryModeRepository.findAll()));    }

    @Override
    public Mono<DeliveryModeDto> getDeliveryModeById(Long id) {
        return Mono.justOrEmpty(deliveryModeMapper.deliveryModeToDTO(deliveryModeRepository.findById(id)));
    }

}
