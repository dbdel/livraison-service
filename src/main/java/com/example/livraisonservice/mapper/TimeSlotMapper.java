package com.example.livraisonservice.mapper;

import com.example.livraisonservice.domain.DeliveryMode;
import com.example.livraisonservice.domain.TimeSlot;
import com.example.livraisonservice.dto.DeliveryModeDto;
import com.example.livraisonservice.dto.TimeSlotDto;
import org.mapstruct.Mapper;
import reactor.core.publisher.Flux;
import springfox.documentation.swagger2.mappers.ModelMapper;

import java.util.Optional;

@Mapper(componentModel = "spring",uses = {ModelMapper.class} )
public interface TimeSlotMapper {


    TimeSlotDto toDto(TimeSlot timeSlot);

    TimeSlot toEntity(TimeSlotDto timeSlotDto);
    default Flux<TimeSlotDto> toDto(Flux<TimeSlot> timeSlotFlux) {
        return timeSlotFlux.map(this::toDto);
    }

    TimeSlotDto timeslotToDTO(TimeSlot timeSlot);

    default TimeSlotDto timeslotToDTO(Optional<TimeSlot> timeSlotOptional) {
        return timeSlotOptional.map(this::timeslotToDTO).orElse(null);
    }
}
