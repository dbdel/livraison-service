package com.example.livraisonservice.mapper;

import com.example.livraisonservice.domain.DeliveryMode;
import com.example.livraisonservice.dto.DeliveryModeDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import springfox.documentation.swagger2.mappers.ModelMapper;

import java.util.List;
import java.util.Optional;

@Mapper(componentModel = "spring",uses = {ModelMapper.class} )

public interface DeliveryModeMapper {


    DeliveryModeDto toDto(DeliveryMode deliveryMode);
    default Flux<DeliveryModeDto> toDto(Flux<DeliveryMode> deliveryModeFlux) {
        return deliveryModeFlux.map(this::toDto);
    }

    DeliveryModeDto deliveryModeToDTO(DeliveryMode deliveryMode);

    default DeliveryModeDto deliveryModeToDTO(Optional<DeliveryMode> deliveryModeOptional) {
        return deliveryModeOptional.map(this::deliveryModeToDTO).orElse(null);
    }
}
